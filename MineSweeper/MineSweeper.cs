using System.Collections.Generic;
using System.Linq;

namespace MineSweeper
{
    internal class MineSweeper
    {
        private const char Mine = '*';
        private readonly string _squares;

        public MineSweeper(string squares)
        {
            _squares = squares;
        }

        public string Compute()
        {
            var resultGrid = new List<string>();

            var lines = new List<string>(_squares.Split('\n'));

            for (var y = 0; y < lines.Count; y++)
            {
                var line = lines[y];
                var result = string.Empty;
                for (var x = 0; x < line.Length; x++)
                {
                    var square = line[x];
                    if (square == Mine)
                        result += Mine;
                    else
                    {
                        result += GetMineCount(x, y, lines);
                    }
                }

                resultGrid.Add(result);
            }

            return string.Join("\n", resultGrid);
        }

        private static int GetMineCount(int x, int y, List<string> lines)
        {
            return lines
                .Where((_, current) => IsNeighbour(current, y))
                .Sum(line => GetHorizontalMineCount(x, line));
        }

        private static int GetHorizontalMineCount(int x, string line)
        {
            return line
                .Where((_, current) => IsNeighbour(current, x))
                .Count(c => c == Mine);
        }

        private static bool IsNeighbour(int current, int target)
        {
            return current <= target + 1 && current >= target - 1;
        }
    }
}