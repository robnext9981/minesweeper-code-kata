﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace MineSweeper
{
    [TestFixture]
    class MinesweeperTests
    {
        [Test]
        public void OneMine()
        {
            var mineSweeper = new MineSweeper("*");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("*"));
        }

        [Test]
        public void OneSafe()
        {
            var mineSweeper = new MineSweeper(".");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("0"));
        }

        [Test]
        public void TwoMines()
        {
            var mineSweeper = new MineSweeper("**");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("**"));
        }

        [Test]
        public void OneMineAndOneSafe()
        {
            var mineSweeper = new MineSweeper("*.");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("*1"));
        }

        [Test]
        public void OneSafeAndOneMine()
        {
            var mineSweeper = new MineSweeper(".*");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("1*"));
        }

        [Test]
        public void TwoMineAndOneSafe()
        {
            var mineSweeper = new MineSweeper("**.");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("**1"));
        }

        [Test]
        public void TwoMineAndTwoSafe()
        {
            var mineSweeper = new MineSweeper("**..");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("**10"));
        }

        [Test]
        public void TwoMinesOnTwoLines()
        {
            var mineSweeper = new MineSweeper("*\n*");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("*\n*"));
        }

        [Test]
        public void OneSafeOnOneMine()
        {
            var mineSweeper = new MineSweeper(".\n*");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("1\n*"));
        }

        [Test]
        public void OneMineBetweenTwoSafe()
        {
            var mineSweeper = new MineSweeper(".\n*\n.");
            Assert.That(mineSweeper.Compute(), Is.EqualTo("1\n*\n1"));
        }

        //[Test]
        //public void OneMineOnThreeLines()
        //{
        //    var mineSweeper = new MineSweeper("*\n.\n.");
        //    Assert.That(mineSweeper.Compute(), Is.EqualTo("*\n1\n0"));
        //}
    }
}